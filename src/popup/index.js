import Vue from 'vue'
import AppComponent from './App/App.vue'
import './App/style.css'

Vue.component('app-component', AppComponent)

import {
  Row,
  Col,
  Button,
  Input,
  Switch
} from 'element-ui'

Vue.use(Row)
Vue.use(Col)
Vue.use(Button)
Vue.use(Input)
Vue.use(Switch)

new Vue({
  el: '#app',
  render: createElement => {
    return createElement(AppComponent)
  }
})
